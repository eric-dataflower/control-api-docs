# Quantum Control Management API Documentation #

To view the documentation, open [public/index.html](public/index.html) in your
browser, or go to:

  _**[https://eric-dataflower.gitlab.io/control-api-docs](https://eric-dataflower.gitlab.io/control-api-docs)**_

## Prerequisites ##

1. Install https://www.npmjs.com/package/redoc-cli.

## Generate ReDoc HTML ##

In the project directory, run the following:

  `redoc-cli bundle schema.yaml --output public/index.html`

## Clarifications with Q-CTRL ##

1. I've generated the documentation using OpenAPI YAML definition into ReDoc,
  and will be hosted as a Github Page on a Github repo, with the HTML and the
  OpenAPI YAML in the repo. Does this sound about right?

  - The document provided states:
  "How you choose to provide this documentation is up to you - as long as the
  team is able to do their job". The choice is all yours!

2. There are ways JSON:API specification can support file uploads, but by no
  means is it standardised in this area. Do the frontend and backend teams
  have a preference?

  - No preference, but it should be done through an api call.

3. There are a few possible types of errors, for example if Maximum Rabi Rate
  exceeds 100. Should I be exhaustively document all error modes and error
  messages? What is the team's expectation?

  - Error objects should follow JSON:API specifications.

4. To delete or retrieve a specific control. What is the unique identifier for
  a control, is it a number, randomly generated string, or the name of the
  control?

  - Please check the Resource Objects - Identification in the JSON:API specs.

## Potential developments ##

- The content of the documentation, [schema.yaml](schema.yaml) may be
  automatically generated depending on the implementation, and comments written
  in the code. This can reduce the effort it takes the team to maintain the
  documentation and reduce the risk the documentation and implementation
  becoming out of sync. The cost is reduced detail, e.g. Some types of error
  messages may not be documented, less detail in response examples.
